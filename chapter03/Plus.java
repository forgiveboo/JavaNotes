public class Plus {
		// Java中'+'号的使用
		public static void main(String[] args) {
				// 第一种使用：当左右两边为数值型时，做加法运算
				System.out.println(100 + 100);

				// 第二种使用：当左右两边有一个是字符串时，做拼接运算
				// 字符串Hello和字符串, world做拼接运算
				System.out.println("Hello" + ", world");						// Hello, world
				// 对于'+'，计算机是做左往右进行运算，所以这里先是字符串Hello和100拼接，然后字符串Hello100再和3拼接
				System.out.println("Hello" + 100 + 3);							// Hello1003
				// 数值100和数值3先做加法运算，然后和字符串Hello做拼接运算
				System.out.println(100 + 3 + "Hello");							// 103Hello
				// 字符串100和数值3做拼接运算
				System.out.println("100" + 3);									// 1003
				// 这里计算机会把字符类型的值转换为ASCLL表中的数值做加法运算，再和字符串做拼接运算
				System.out.println('H' + 'e' + 'l' + 'l' + 'o' + ", world");	// 500, world.
		}
}
