public class Variable01 {
		// 变量使用的基本步骤
		public static void main(String[] args) {
				// 声明变量
				int a;
				// 给变量赋值
				a = 90;
				// 使用变量名
				System.out.println(a);
				
				// 声明变量和给变量赋值可以一步搞定
				int b = 100;
				System.out.println(b);
		}
}
