public class ForceConvertDetail {
		// 强制类型转换注意事项
		public static void main(String[] args) {
				// 强转符号()只对离()最近的操作数有效,往往需要使用()提升表达式的优先级
				/* int n = (int)10 * 1.5 + 15 * 1.5;会报类型不兼容错误，
				 * 因为这个表达式只是把10转成了int类型，
				 * 1.5还是double类型，那么整个表达式的类型就应该是double类型
				 */
				int n = (int)(10 * 1.5 + 15 * 1.5);    // 用()小括号提升表达式的优先级，计算机会先计算表达式，再把结果转换为int类型
				System.out.println(n);

				// char类型可以保存int类型的常量值，但是不能把int类型的变量赋值给char,要将int类型的变量赋值给char类型，需要强制转换
				char c = 100;
				System.out.println(c);
				int n1 = 100;
				// char c2 = n1;会报类型不兼容错误，因为你n1是int类型，c2是char类型，char类型不能自动转换为int类型
				char c1 = (char)n1;
				System.out.println(c1);
		}
}
