public class StringToBasic {
		// String转换为对应的基本数据类型
		public static void main(String[] args) {
				/* 每个基本数据类型都有一个对应的包装类，
				 * 通过基本数据类型的包装类调用相应的parseXX(对应的基本数据类型)方法将字符串转换为对应的基本数据类型
				 */
				String s = "123";
				int n1 = Integer.parseInt(s);
				double n2 = Double.parseDouble(s);
				// 以此类推，每一个基本数据类型都可以通过这种方式将String类型转换为对应的基本数据类型
				double n3 = n1 + n2;
				System.out.println(n3);

				// 需要注意的是：boolean也可以把String类型转换为boolean类型，只是String类型的值必须是true或者false
				boolean b = Boolean.parseBoolean("true");

				// char类型转换为String类型-->取出字符串中的单个字符
				char c = s.charAt(0);	// 取出s这个字符串中的第一个字符
				System.out.println(c);
		}
}
