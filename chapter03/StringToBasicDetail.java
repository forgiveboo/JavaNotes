public class StringToBasicDetail {
		// Stirng类型转基本数据类型细节
		public static void main(String[] args) {
				// 将String类型转换为基本数据类型时，要确保String类型能被转为有效的数据
				// 如果字符串是由纯数字组成，那么可以转换
				String s = "123";
				double d = Double.parseDouble(s);
				System.out.println(d);
				// 如果字符串是由字母组成，那么程序就会抛出异常而终止
				String s1 = "Hello";
				// double n1 = Double.parseDouble(s1);编辑可以通过，但是运行时程序会抛出异常，因为Hello不能被转换为有效的数据
		}
}
