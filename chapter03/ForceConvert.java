public class ForceConvert {
		// 强制类型转换：是自动类型转换的逆过程
		public static void main(String[] args) {
				// 强制类型转换使用强制类型转换符()
				int n = (int)1.9;
				System.out.println(n);	// 输出1,强制类型转换会造成精度损失
				int n1 = 1000;
				byte n2 = (byte)n1;
				System.out.println(n2);	// 输出-24,强制类型转换会造成数据溢出
		}
}
/*
 * 强制类型转换会造成精度损失和数据溢出
 * 除非你知道自己在做什么，否则要格外小心
 * */
