public class FloatDetail {
		// 浮点型使用细节
		public static void main(String[] args) {
				// 浮点型常量默认为double型
				double a = 5.12;
				// 要声明float型需在后面加f或F
				float b = 5.12F;
				// float b = 5.12; 会报类型不兼容错误，因为默认常量为double型
				// 如果小数点前面是0(类似于0.123)，0可以省略
				double c = .123;	// 等价于double c = 0.123

				// 浮点数有两种表现形式：
				// 1. 十进制	1.123
				// 2. 科学计数法	5.12E2(5.12 * 10 ^ 2)	5.12E-2(5.12 / 10 ^ 2)
				System.out.println(5.12E2);

				// 一般推荐使用double类型，因为精度更高(能保留的小数位更多)
				double n = 1.23456789;
				float n1 = 1.23456789F;
				System.out.println(n);	// 1.23456789
				System.out.println(n1);	// 1.2345679

				// 浮点数的运算结果是近似值，所以当我们对运算结果进行相等判断时，要小心，有可能不相等
				double num = 2.7;
				double num1 = 8.1 / 3;
				if (num == num1) {
						System.out.println("num == num1");
				}
				else {
						System.out.println("num != num1");
				}

				// 应该判断两个数的差值的绝对值是否在某个范围内
				if (Math.abs(num - num1) < 0.000001) {	// Math.abs()可以通过Java API查看
						System.out.println("差值非常小，到了安全范围");
				}
		}
}
