public class Homework02 {
		// 课堂测验2
		public static void main(String[] args) {
				// 使用char类型，分别保存并输出\n,\t,\r,\\,1,2,3等字符
				char c1 = '\n';
				System.out.println(c1);
				char c2 = '\t';
				System.out.println(c2);
				char c3 = '\r';
				System.out.println(c3);
				char c4 = '\\';
				System.out.println(c4);
				char c5 = '1';
				System.out.println(c5);
				char c6 = '2';
				System.out.println(c6);
				char c7 = '3';
				System.out.println(c7);
		}
}
