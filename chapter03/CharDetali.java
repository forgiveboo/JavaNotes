public class CharDetali {
		// 字符类型(char):用于存放单个字符，占用2个字节
		public static void main(String[] args) {
				char c = 'a';
				char c1 = '王';
				char c2 = '\t';	// 字符类型可以存放转义字符
				char c3 = 97;	// 字符类型可以直接存放一个数值

				System.out.println(c);	// a
				System.out.println(c1);	// 王
				System.out.println(c2);	// 制表符
				System.out.println(c3);	// 计算机会先把数值97转换为对应的Unicode码，再和10相加a

				// char类型的数据的本质是数值，可以进行数值运算，每个字符都有对应的ACSII码
				char c4 = '王';
				char c5 = '憨';
				char c6 = '憨';
				System.out.println(c4 + c5 + c6);
				System.out.println('a' + 10);	// 107,计算机会先把字符a转换为对应的ACSII码，再和10相加

				// 当给char类型的变量赋值一个字符时，如果想输出字符所对应的数值，需要用int()进行数据类型转换
				char c7 = 'b';
				System.out.println((int)c7);	// 将输出结果转换成int类型
				
				char c8 = 'a' + 1;
				System.out.println((int)c8);	// 98
				System.out.println(c8);			// 计算机会先把字符a转换为对应的ACSII码，再和1相加=98，再输出98对应的Unicode码
				// 字符类型的变量不能接收浮点类型的数值，会报无法将double类型转换为char类型
				// char c8 = 98.5;
		}
}
