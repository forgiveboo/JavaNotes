public class Variable02 {
		// 使用变量的不同数据类型体现一个人的基本信息
		public static void main(String[] args) {
				// 用整型表示年龄
				int age = 23;
				// 用浮点型表示成绩
				double grouds = 88.6;
				// 用字符型表示性别‘
				char gender = '女';
				// 用字符串型表示名字
				String name = "王憨憨";

				System.out.println("年龄:" + age + "  成绩:" + grouds + "  性别:" + gender + "  姓名:" + name);
		}
}
