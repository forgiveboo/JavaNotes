public class AutoConvert {
		// 基本数据类型的转换：低精度-->高精度可以自动转换
		public static void main(String[] args) {
				// 自动转换路径1：char->int->long->float->double
				int c = 'a';			// 'a'会自动转换为int类型
				System.out.println(c);	// 输出97
				// 自动转换路径2：byte->short->int->long->float->double
				double d = 80;			// 80会自动转换为double类型
				System.out.println(d);	// 输出80.0
		}
}
