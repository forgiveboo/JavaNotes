public class Homework04 {
		// 课堂测验4
		public static void main(String[] args) {
				// 输出姓名 年龄 性别 成绩 爱好
				// 	   XX	XX	 XX   XX   XX
				String name = "王憨憨";
				int age = 23;
				char gender = '女';
				double score = 96.8;
				String hobby = "看书";
				System.out.println("姓名\t年龄\t性别\t成绩\t爱好\n" + name + '\t' + /*输出的信息可以用一个字符串写完，
								每个信息中间加个'\t'即可*/
								age + '\t' + gender + '\t' + score + '\t' + hobby);
		}
}
