public class BasicToString {
		// 基本数据类型转换为String类型
		public static void main(String[] args) {
				// 将基本数据类型的值+""即可
				int n = 100;
				float n1 = 4.5F;
				double n2 = 8.9;
				boolean n3 = true;
				String s = n + "";
				String s1 = n1 + "";
				String s2 = n2 + "";
				String s3 = n3 + "";
				System.out.println(s + " " + s1 + " " + s2 + " " + s3);
		}
}
