public class IntDetail {
		// 整型的具体细节
		public static void main(String[] args) {
				// 整型常量默认是int类型,除非数值过大
				int a = 1;
				// 要声明long型常量，要在后面加l或L
				long b = 1L;
				// int b = 1L; 这样会报不兼容的类型，因为a是lang型，而声明时用的int型
		}
}
