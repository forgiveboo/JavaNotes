public class AutoConvertDetail {
		// 自动类型转换的使用细节
		public static void main(String[] args) {
				// 有多种数据类型混合运算时，系统会自动将所有数据类型自动转换为算式中精度(容量)最大的数据类型再进行计算
				int n = 10;
				double d = n + 11.1;	// n会被自动转换为double类型
				System.out.println(d);
				// float f = n + 11.1; 会报类型不兼容错误，因为11.1是double类型，不能转换为float型
				float f1 = n + 11.1F;	// 这是对的，因为11.1F是float类型，同时n也会被转换为float类型
				System.out.println(f1);

				// (byte, short)和char之间不发生自动转换
				short s = 's';
				System.out.println(s);
				byte b = 10;	// 当把一个数赋值给byte时，计算机会先判断该数是不是在byte的范围内，如果是就可以
				int n1 = 10;
				// byte b1 = n1;如果是给byte赋值一个int类型的变量会报类型不兼容错误，
				// 因为n1是int型，b1是byte类型，计算机会先判断数据类型，int不能自动转换到byte类型
				System.out.println(b);
				System.out.println(n1);
				// char c = b;会报类型不兼容错误，因为c是char类型，而b是byte类型，这两者之间不能进行类型转换

				// byte,short,char之间可以进行运算，计算结果会把结果的数据类型转换为int型
				char c1 = 'a';
				short s1 = 1000;
				byte b1 = 100;
				// b1 = c1;会报类型不兼容错误，因为byte和char之间不能自动转换
				// short n2 = s1 + b1;会报类型不兼容错误，因为计算机会首先把数据类型转换为int，而int不能自动转换为short
				System.out.println(c1 + s1 + b1);

				// boolean不参与自动类型转换
				// boolean b2 = true;
				// int n2 = b2;会报类型不兼容错误，因为boolean不参与自动类型转换
				
				// 自动提升原则：表达式结果的类型会自动转换为操作数中精度最大的类型
				byte num1 = 10;
				short num2 = 100;
				int num3 = 10000;
				double num4 = 1000000;
				double num5 = num1 + num2 + num3 + num4;
				// float num5 = num1 + num2 + num3 + num4;会报类型不兼容错误，因为表达式中精度最高的是double，float的精度低于double
				System.out.println(num5);
		}
}
