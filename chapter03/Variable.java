public class Variable {
		// 变量的使用
		public static void main(String[] args) {
				int i = 1;	// 定义了一个变量，类型int，名称i，值1
				System.out.println(i);	// 输出i变量的值
				int j = 3;	// 定义了一个变量，类型int，名称j，值3
				System.out.println(j);	// 输出j变量的值 3
				j = 89;		// 把89赋值给变量j
				// j = 'c';	// 不能把char类型的值赋值给int类型，会报类型不兼容的错误
				// 变量的值只能在同一数据类型之间变化
				System.out.println(j);	// 输出j变量的值 88
		}
}
