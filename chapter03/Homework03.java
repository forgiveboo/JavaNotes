public class Homework03 {
		// 课堂测验3
		public static void main(String[] agrs) {
				// 保存两个性别用+拼接并输出
				char c1 = '男';
				char c2 = '女';
				System.out.println(c1 + c2);	// 输出'男'对应的Unicode码值+'女'对应的Unicode码值
				// 保存两个书名用+拼接并输出
				String s3 = "深入理解计算机系统";
				String s4 = "算法导论";
				System.out.println(s3 + " " + s4);	// 输出两个字符串的拼接结果
				// 保存两个书的价格并输出
				double d1 = 109.8;
				double d2 = 39.9;
				System.out.println(d1 + d2);		// 输出的d1+d2的值
		}
}
