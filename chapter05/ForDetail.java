public class ForDetail {
    // for循环细节
    /*
    * 1.循环条件是返回布尔值的关系表达式
    * 2.for(;循环条件;){} for循环的循环变量初始化和变量迭代可以写在其他地方，但小括号中的两个;分号不能省略
    * 3.循环变量初始化可以有多条初始化语句，但要求数据类型一样，并且中间用逗号隔开。
    * 4.循环变量的迭代也可以有多条语句，中间用逗号隔开
    * */
    public static void main(String[] args) {
        // 细节3,4
        int count = 3;
        for (int i = 0, j = 0; i < count; i++, j += 2) {
            System.out.println("i = " + i + ", j = " + j);
        }

        // 细节2
        int i = 1;  // 如果后面的代码还会用到循环变量i，那么就把它的初始化写在for循环的外面以便后续使用
        for (; i < 11; ) {    // 如果循环变量的初始化在for循环的小括号里，那么这个循环变量只能在该for循环中使用
            System.out.println("Hello");
            i++;    // 循环变量的迭代
        }
        System.out.println("i = " + i); // i = 11

        // 死循环,只要程序不崩或者不人为干涉,循环就会一直执行
        // for(;;) {
        // }
    }
}
