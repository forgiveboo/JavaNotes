import java.util.Scanner;

public class SwitchExercise02 {
    // switch练习2
    public static void main(String[] agrs) {
        // 对学生成绩进行判断，大于60分则输出合格，小于60分输出不合格
        /*
        * 1.思路分析：
        * (1)创建Scanner类对象接收用户输入
        * (2)定义double类型变量double score存储用户输入
        * (3)用if语句判断用户输入的成绩是否正确，score>=0&&score<=100
        * (4)在成绩合格的前提下，根据用户输入的成绩用switch语句判断是否合格，score/60是否大于1，case 1输出成绩合格，case 0输出成绩不合格
        * 2.代码实现
        */
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入学生成绩：");
        double score = sc.nextDouble();
        if (score >= 0 && score <= 100) {
            switch ((int)score / 60) {
                case 0:
                    System.out.println("成绩不合格");
                    break;
                case 1:
                    System.out.println("成绩合格");
                    break;
            }
        } else {
            System.out.println("请输入正确的成绩");
        }
    }
}
