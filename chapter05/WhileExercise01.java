public class WhileExercise01 {
    // while循环练习1
    public static void main(String[] args) {
        // 打印1-100之间所有能被3整除的整数
        /*
        * 1.编程思想：
        * (1)化繁为简
        * <1>先打印1-100之间所有的整数
        * <2>对打印出来的数进行数据过滤-->添加一个if判断，if(i % 3 == 0)就把该数打印出来
        * */
        int i = 1;  // 循环变量初始化
        while ( i <= 100) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }
    }
}
