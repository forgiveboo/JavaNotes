import java.util.Scanner;

public class If03 {
		/* 多分支结构：
		 * 基本语法：
		 * if(条件表达式1){
		 *		代码块1;
		 * } else if (条件表达式2) {
		 *		代码块2;
		 * } 
		 * ... (可以有多个else if) 
		 * else {
		 * 		代码块3;
		 * }
		 * 执行流程：
		 * 1.当条件表达式1成立时，执行代码块1，执行完毕后跳出分支结构
		 * 2.当条件表达式1不成立时，判断条件表达式2，如果条件表达式2成立，执行代码块2，执行完毕后跳出分支结构
		 * 3.当所有的条件表达式都不成立时，执行代码块3并继续执行后续代码
		 */
		public static void main(String[] args) {
				// 案例：输入王憨憨的芝麻信用分
				// 如果：
				// 1. 信用分为100，输出信用极好
				// 2. 信用分为(80, 99],输出信用优秀
				// 3. 信用分为[60, 80],输出信用一般
				// 4. 其他情况，输出信用不及格
				// 请从键盘输入信用分并加以判断
				/*
				 * 1.思路分析：
				 * (1)声明一个Scanner类的实例用以用户输入
				 * (2)定义一个double类型的变量存储用户输入信息
				 * (3)用if-else if-else语句对信息进行判断并输出对应结果
				 */
				
				Scanner sc = new Scanner(System.in);
				System.out.print("请输出芝麻信用分(1-100)：");
				double creditScore = sc.nextDouble();

				// 对输入的数据进行判断
				if (creditScore < 1 || creditScore > 100) {
						System.out.println("输入有误");
				} else if (creditScore == 100) {
						System.out.println("信用极好");
				} else if (creditScore > 80 && creditScore <= 99) {
						System.out.println("信用优秀");
				} else if (creditScore >= 60 && creditScore <= 80) {
						System.out.println("信用一般");
				} else {
						System.out.println("信用不及格");
				}
				sc.close();
		}
}
