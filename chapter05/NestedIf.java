import java.util.Scanner;

public class NestedIf {
		// 嵌套分支：
		// 在一个完整的分支结构中再使用一个(多个)完整的分支结构
		// 基本语法：
		// if () {
		// 		if () {
		//
		// 		} else {
		//
		// 		}
		// } else {
		//
		// }
		public static void main(String[] args) {
				// 案例：
				// 参加歌手比赛，如果初赛成绩大于8.0则进入决赛，否则淘汰。
				// 并且根据性别提示进入男子组或女子组。输入性别和成绩，进行判断和输出信息
				/*
				 * 1.思路分析：
				 * (1)接收用户输入成绩和性别，成绩score用double类型定义，性别gender用char类型定义
				 * (2)判断score是否大于8.0,是则提示进入决赛,不是则提示淘汰
				 * (3)如果进入决赛,再对性别进行判断(使用分支嵌套)
				 */
				Scanner sc = new Scanner(System.in);
				System.out.print("请输入选手成绩：");
				double score = sc.nextDouble();
				// 判断成绩
				if (score >= 8.0) {
						System.out.print("该选手进入决赛，请输入选手性别进行分组：");
						char gender = sc.next().charAt(0);
						// 判断性别
						if (gender == '男') {
								System.out.println("该选手分到男子组");
						} else if (gender == '女') {
								System.out.println("该选手分到女子组");
						} else {
								System.out.println("输入的性别有误");
						}
				} else {
						System.out.println("该选手被淘汰了");
				}
				sc.close();
		}
}
