public class While01 {
    // while循环基本语法
    /*
    * 循环变量初始化
    * while(循环条件){
    *   循环体
    *   循环变量迭代
    * }
    * */
    public static void main(String[] args) {
        // 输出10句Hello
        int i = 1;  //循环变量
        while (i <= 10) {   // 循环条件
            System.out.println("Hello");    // 循环体
            i++;
        }
        System.out.println("循环结束");
    }
}
