import java.util.Scanner;

public class If01 {
	/* 单分支：
	if(条件表达式) {
		代码块;		
	}
	如果条件表达式为true，那么执行代码块，如果为false，那么跳过代码块继续
	执行之后的代码
	*/
	// 案例：输入一个人的年龄，如果大于18岁则送进监狱
	public static void main(String[] args) {
		/*
		1.需求分析：接收一个用户输入的int类型数值，判断是大于18还是小于18，大于18则输出"你已经成年，送入监狱"，小于18则跳过
		2.思路分析：创建Scanner类的事例来接收用户输入，再用if语句来判断数值是否大于18
		3.上代码
		*/
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入年龄：");
		int age = sc.nextInt();
		if (age >= 18) {
			System.out.println("你已经成年了，送进监狱！");
		}
		sc.close();
		System.out.println("程序继续...");
	}
}