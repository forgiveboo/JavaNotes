import java.util.Scanner;

public class Switch01 {
    // switch案例
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入a-g其中一个字母：");
        char week = sc.next().charAt(0);

        switch (week) {
            case 'a':
                System.out.println("今天星期一");
                break;
            case 'b':
                System.out.println("今天星期二");
                break;
            case 'c':
                System.out.println("今天星期三");
                break;
            case 'd':
                System.out.println("今天星期四");
                break;
            case 'e':
                System.out.println("今天星期五");
                break;
            case 'f':
                System.out.println("今天星期六");
                break;
            case 'g':
                System.out.println("今天星期天");
                break;
            default:
                System.out.println("请输入a-g之间的字母");
                break;
        }
    }
}
