import java.util.Scanner;

public class SwitchExercise01 {
    // switch练习1
    public static void main(String[] args) {
        // 把小写的a,b,c,d,e转换为大写的，键盘输入小写字母，输出大写字母
        /*
        * 1.思路分析：
        * (1)创建Scanner对象接收用户输入
        * (2)定义char类型变量char c存储用户输入
        * (3)用switch语句，根据用户输入的小写字母输出对应的大写字母
        * 2.代码实现
        */
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入a-e的小写字母：");
        char c = sc.next().charAt(0);
        switch (c) {
            case 'a':
                System.out.println("A");
                break;
            case 'b':
                System.out.println("B");
                break;
            case 'c':
                System.out.println("C");
                break;
            case 'd':
                System.out.println("D");
                break;
            case 'e':
                System.out.println("E");
                break;
            default:
                System.out.println("请输入正确的字母");
        }
        sc.close();
    }
}
