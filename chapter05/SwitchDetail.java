public class SwitchDetail {
    // switch语句细节和注意事项
    /*
     * 1.表达式的数据类型必须和case后面的常量的数据类型一致，或是可以自动转换为可以相互比较的类型
     * 2.switch(表达式)，表达式中的返回值必须是char,byte,short,int,enum(枚举),String类型，其他类型不可以
     * 3.case后面必须跟常量值，不能跟变量
     * 4.default语句是可选的，当case没有匹配时会执行default语句，如果没有default语句，会直接跳出switch语句
     * 5.break语句用于执行完case语句块后跳出switch语句，如果在switch结构中一个break语句都没有，
     *   那么程序会顺序执行完所有switch中的语句块，不会判断case后面的常量值，直接执行case后面的语句块，这种现象叫穿透
     *   如果第一个case语句没有匹配，那么穿透现象从第二条case语句开始，直到遇到break或switch语句结束
     *   如果一个case语句都没有匹配，那么不管前面的case语句块中有没有break语句，程序会直接执行default语句(如果有)，然后跳出switch语句
     */
    public static void main(String[] args) {
        // 细节1
        char c = 'a';
        switch (c) {
            case 'a':
                System.out.println("ok1");
                break;
            // case 'Hello': String类型无法自动转换为char类型
            //    System.out.println("ok2");
            //    break;
            case 20:    // 可以转为相互比较的类型
                System.out.println("ok2");
                break;
            default:
                System.out.println("ok3");
                break;
        }
        // 细节2
        //double d = 1.1;
        //switch (d) {错误，switch语句不支持double类型

        //}
        // 细节3
        // switch (c) {
            // case c: 错误，case后面只能跟常量值
        //  }
        // 细节4
        switch (c) {
            case 'h':
                System.out.println("ok4");
                break;
            //default:
            //    System.out.println("ok2");可以没有default语句，在这里程序会直接跳出switch语句，因为没有匹配到任何case语句块
        }
        // 细节5
        switch (c) {
            case 'e':
                System.out.println("ok5");  // 不会执行，因为没有匹配到'a'
            case 'b':
                System.out.println("ok6");  // 不会执行，因为前一个case语句没有被匹配
            case 'c':
                System.out.println("ok7");  // 不会执行，同样，前一个case语句没有被匹配
            case 'a':
                System.out.println("ok8");  // 会执行，因为匹配到了'a'，后面没有break语句，开始穿透
            case 'f':
                System.out.println("ok9");  // 会执行，因为前一个case语句块中没有break语句
            case 'g':
                System.out.println("ok10"); // 会执行，因为前一个case语句块中没有break语句
                break;                      // 程序会从这里跳出switch语句
            default:
                System.out.println("ok11"); // 不会执行，因为前面遇到了break语句
        }
    }
}
