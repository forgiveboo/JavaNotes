import java.util.Scanner;

public class If02 {
	/*
	双分支：if-else
	基本语法：
		if(条件表达式){
			代码块1;
		} else {
			代码块2;
		}
	说明：当条件表达式为true时，执行代码块1，否则执行代码块2
	*/
	// 案例:用户输入年龄，如果年龄超过18岁，送进监狱，否则放过他
	public static void main(String[] args) {
		/*
		1.需求分析：接收用户输入的年龄，并判断是大于18岁还是小于18岁，大于18岁送进监狱，小于18岁则放过他
		2.思路分析：<1>创建Scanner实例来请求用户输入
				  <2>定义一个int类型变量来接收用户的输入信息。
				  <3>用if-else语句来判断输入的年龄是大于18岁还是小于18岁
		3.上代码
		*/
		// 创建Scanner类的实例
		Scanner sc = new Scanner(System.in);
		// 请求用户输入
		System.out.print("请输入年龄：");
		// 接收用户输入
		int age = sc.nextInt();
		// 条件判断
		if (age >= 18) {
			System.out.println("你已经成年了，要为自己的行为负责，进监狱吧！");
		} else {
			System.out.println("你还小，不用进监狱，但不要再干坏事了！");
		}
		sc.close();
	}
}