public class IfExercise02 {
		// 分支结构练习2
		public static void main(String[] agrs) {
				// 需求：定义两个变量，判断二者的和是否能被3又能被5整除
				/*
				 *1.思路分析：
				 <1>定义两个int类型变量num1和num2并赋值
				 <2>再定义一个int类型变量来保存num1+num2的值
				 <3>用if-else语句判断num3 / 3 == 0 && num3 /5 == 0的值
				 <4>如果条件为true，if语句的代码块中输出能被3也能被5整除的信息
				 <5>如果条件为flase，if语句的代码块中输出不能同时被3和5整除的信息
				 *2.上代码
				 */
				int num1 = 10;
				int num2 = 67;
				int num3 = num1 + num2;
				// 判断
				if (num3 % 3 == 0 && num3 % 5 == 0) {
						System.out.println("同时能被3和5整除");
				} else {
						System.out.println("不能同时被3和5整除");
				}
		}
}
