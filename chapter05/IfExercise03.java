import java.util.Scanner;

public class IfExercise03 {
		// 分支结构练习3
		public static void main(String[] args) {
				// 需求：判断一个年份是否是闰年
				/*
				 * 1.思路分析：
				 * <1>创建Scanner类的实例请求用户输入
				 * <2>定义一个int类型的变量year接收用户的输入信息
				 * <3>判断条件year % 4 == 0 && year % 100 != 0 && year % 400 == 0
				 * <4>用if-else语句控制程序输出对应结果
				 */
				Scanner sc = new Scanner(System.in);
				System.out.print("请输入一个年份：");
				int year = sc.nextInt();
				if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
					System.out.println(year + "是闰年");
				} else {
						System.out.println(year + "不是闰年");
				}
				sc.close();
		}
}
