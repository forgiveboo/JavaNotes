public class For01 {
    // for循环
    // 循环：就是可以使某段代码重复执行
    public static void main(String[] args) {
        // 打印10句Hello, world
        // 传统方法：执行10个输出语句
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
//        System.out.println("Hello, world");
        // 很显然，传统的方法并不可取，如果要打印大量语句，代码肯定不能这么写
        // for循环的方法
        for (int i = 1; i <= 10; i++) {
            System.out.println("Hello, world");
        }
    }
}
