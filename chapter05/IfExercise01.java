public class IfExercise01 {
	// 分支结构练习1
	public static void main(String[] args) {
		/*
		需求：声明两个double类型变量并赋值
			 判断第一个数大于10.0且第二个数小于20.0，打印两数之和
		1.需求分析：定义两个double类型变量并赋值
				  如果第一个数大于10.0且第二个数小于20.0，打印两个数的和
		2.思路分析：<1>定义两个double类型变量并赋值
				  <2>用if语句判断第一个数大于10.0且第二个数小于20.0
				  <3>如果条件成立，输出两个数的和
		3.上代码
		*/
		// 定义两个double类型变量并赋值
		double num1 = 11.2;
		double num2 = 19.8;
		// 用if语句判断
		if (num1 > 10.0 && num2 < 20.0) {
			System.out.println(num1 + num2);
		}
		System.out.println("判断完毕");
	}
}