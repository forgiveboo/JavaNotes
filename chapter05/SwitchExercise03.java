import java.util.Scanner;

public class SwitchExercise03 {
    // switch练习3
    public static void main(String[] agrs) {
        // 根据用户输入的月份，输出该月份对应的季节。3,4,5为春季，6,7,8为夏季，9,10,11为秋季，12,1,2为冬季
        /*
         * 1.思路分析：
         * (1)创建Scanner类对像接收用户输入
         * (2)定义int类型变量int month存储用户输入
         * (3)用switch语句根据用户的输入判断该月为哪个季节(使用穿透)
         * 2.代码实现
         */
        Scanner sc = new Scanner(System.in);
        System.out.print("请输入月份：");
        int month = sc.nextInt();
        switch (month) {
            case 3:
            case 4:
            case 5:
               System.out.println("该月为春季");
               break;
            case 6:
            case 7:
            case 8:
               System.out.println("该月为夏季");
               break;
            case 9:
            case 10:
            case 11:
               System.out.println("该月为秋季");
               break;
            case 12:
            case 1:
            case 2:
                System.out.println("该月为冬季");
                break;
            default:
                System.out.println("你输入的月份有误");
        }
    }
}
