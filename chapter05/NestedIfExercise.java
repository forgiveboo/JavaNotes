import java.util.Scanner;

public class NestedIfExercise {
		// 嵌套分支练习
		public static void main(String[] args) {
				// 出票系统：根据淡旺季的月份和年龄，打印票价
				// 4-10月为旺季：
				// 成人(18-60岁)：60
				// 儿童(0-18岁)：半价
				// 老人(大于60岁)：1/3
				// 其他月为淡季：
				// 成人：40
				// 其他：20
				/*
				 * 1.思路分析
				 * (1)创建Scanner对象接收用户输入的月份，并使用int mounth来存储
				 * (2)根据输入的月份判断该月为淡季还是旺季
				 * (3)再接收用户输入的年龄，并使用int age来存储
				 * (4)根据对年龄的判断输出对应年龄段的票价
				 * 2.代码实现==>思路-->java代码
				 */
				Scanner sc = new Scanner(System.in);
				System.out.print("请输入本月月份(1-12)：");
				int month = sc.nextInt();
				System.out.print("请输入您的年龄(大于0)：");
				int age = sc.nextInt();
				if (month >= 4 && month <= 10) {	// 判断月份属于淡季还会旺季
						if (age >= 18 && age < 60) {	// 判断年龄阶段
								System.out.println("请您支付60元");
						} else if (age < 18) {
								System.out.println("请您支付30元");
						} else {
								System.out.println("请您支付20元");
								}
				} else if ((month < 4 && month > 0) || (month > 10 && month < 12)) {
						if (age >= 18 && age < 60) {
								System.out.println("请您支付40元");
						} else {
								System.out.println("请您支付20元");
						}
				} else {
						System.out.println("您输入的月份有误");
				}
				sc.close();
		}
}
