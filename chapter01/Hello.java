// 这是java的快速入门，演示java的开发步骤

// 对代码的相关说明：
//1. public class Hello 表示Hello是一个类，是一个public公有的类
//2. Hello{} 表示一个类的开始和结束
//3. public static void main(String[] args) 表示一个主方法
//4. main() {} 表示方法的开始和结束
//5. System.out.println("Hello, world"); 表示输出"Hello, world"到控制台
//6. ;表示语句结束
public class Hello {
		// 编写一个main方法
		public static void main(String[] args) {
				System.out.println("Hello, world");
		}
}

// 一个源文件只能有一个public类，其余类数量不限
// 每一个类在编译之后都会生成对应的class文件

// 这是一个类
class Dog{
		public static void main(String[] args) {
				System.out.println("Hello, dog");
		}
}
// 这也是一个类
class Tigger{
		public static void main(String[] args) {
				System.out.println("Hello, tigger");
		}
}
