/*
 * 演示注释用法
 * 注释可以：
 * 1.提高代码可读性
 * 2.先通过自己的思路通过注释整理出来，再写代码
 * 注释类型：
 * 1.单行注释
 * 2.多行注释
 * 3.文档注释
 * *:多行注释不可以嵌套
 */
public class Comment {
		public static void main(String[] args) {
				// 单行注释
				// 先定义变量
				int i = 10;
				int j = 20;
				// 把i和j加起来并定义一个变量保存结果
				int sum = i + j;
				// 输出结果
				System.out.println("结果是：" + sum);

				/*
				 *多行注释
				 */

				/**
				 * 文档注释
				 * @author	王憨憨
				 * @version	1.0
				 * */
		}
}
