// 演示转义字符的使用
public class ChangeChar {
		public static void main(String[] args) {
				// \t:制表符
				System.out.println("北京\t天津\t上海");
				// \n:换行符
				System.out.println("hack\nhello\nworld");
				// \\:一个\
				System.out.println("新技术\\新知识");
				// \":一个单引号
				System.out.println("王憨憨说：\"我要好好学Java\"");
				// \'':一个双引号
				System.out.println("\'c\'");
				// \r:一个回车。回车会把光标定在字符串的第一个字符上，然后用回车之后的字符替换字符串的开头
				System.out.println("王憨憨\r是憨憨");
				// \r\n:一个回车一个换行。回车之后直接换行
				System.out.println("王憨憨\r\n是憨憨");
		}
}
//1. \t:制表符，实现对齐功能
//2. \n:换行符，实现换行
//3. \":一个双引号
//4. \':一个单引号
//5. \r:一个回车
//6. \\:一个\
