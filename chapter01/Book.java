/**
 * 输出以下格式：
 * 书名 	作者 	价格 	销量
 * 三国		罗贯中	120		1000	
 *
 * */
public class Book {
		public static void main(String[] args) {
				System.out.println("书名\t\t作者\t\t价格\t\t销量\n三国\t\t罗贯中\t\t120\t\t1000");
		}
}
