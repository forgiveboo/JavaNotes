public class ProhibitTest {
	// 演示进制
	public static void main(String[] args)  {
		// int bin = 0b1010;	// 二进制
		// int dec = 1010;		// 十进制
		// int oct = 01010;	// 八进制
		// int dex = 0x10101;	// 十六进制

		// System.out.println("bin = " + bin);	// 10
		// System.out.println("dec = " + dec);	// 1010
		// System.out.println("oct = " + oct);	// 520
		// System.out.println("dex = " + dex);	// 65793

		int n1 = 0b110001100;
		int n2 = 02456;
		int n3 = 0xA45;

		System.out.println(n1);
		System.out.println(n2);
		System.out.println(n3);
	}
}