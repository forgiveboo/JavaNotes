public class LogicOperator02 {
		// 逻辑或(|)和短路或(||)的区别
		public static void main(String[] args) {
				/*
				 * 逻辑或(|):不管第一个条件是不是为true，都要判断第二个条件
				 * 短路或(||):如果第一个条件为true，则关系表达式的值就为true，不会判断第二个条件
				 */
				// 逻辑或执行步骤:
				int a = 10;
				int b = 20;
				if (a > b | a == 20) {
						/*
						 * 判断a > b的结果-->false
						 * 再判断a == b的结果-->false
						 * 关系表达式结果-->false
						 */
						System.out.println("逻辑或");
				} else {
						System.out.println("逻辑或效率低");
				}
				// 短路或执行步骤:
				if (a < b || a > b) {
						/*
						 * 判断a < b的结果-->true
						 * 关系表达式结果-->true
						 */
						System.out.println("短路或");
				} else {
						System.out.println("短路或效率高");
				}
		}
}
