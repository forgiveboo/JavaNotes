public class BinaryTest {
		// 演示四种进制
		public static void main(String[] args) {
				int bin = 0b1010;	// 二进制
				int oct = 01010;	// 八进制
				int dec = 1010;		// 十进制
				int hex = 0x10101;	// 十六进制

				System.out.println(bin);
				System.out.println(oct);
				System.out.println(dec);
				System.out.println(hex);
		}
}
