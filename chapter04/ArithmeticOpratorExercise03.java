public class ArithmeticOpratorExercise03 {
		// 课堂练习
		public static void main(String[] args) {
				/*
				 * 练习1:
				 * 假设还有59天放假，问还有多少个星期零多少天
				 */
				/*
				 * 1.需求分析：
				 * 		(1)59中有多少个7
				 * 		(2)59中除去x个7后还剩多少
				 * 		(3)输出结果
				 * 2.思路分析：
				 * 		(1)定义三个int类型的变量分别存放holiday(59),weeks(星期数),days(天数)
				 * 		(2)59 / 7 刚好可以得到59中有多少个7(星期数)
				 * 		(3)59 % 7 刚好可以得到除去星期数剩下的天数(天数)
				 * 3.代码编写
				 */
				int holiday = 59;
				int weeks = holiday / 7;
				int days = holiday % 7;
				System.out.println("还有" + weeks + "个星期零"+ days + "天");

				/*
				 * 练习2:
				 * 定义一个变量保存华氏温度，求出华氏温度对应的摄氏温度
				 * 华氏温度转摄氏温度公式：5/9*(华氏温度-100)
				 */
				/*
				 * 1.需求分析：
				 * 		(1)定义一个变量保存华氏温度
				 * 		(2)根据公式将华氏度温度转为摄氏度温度
				 * 		(3)输出结果
				 * 2.思路分析：
				 * 		(1)定义两个double类型的变量F存放(华氏度温度)和C(摄氏度温度)
				 * 		(2)根据公式5/9*(华氏温度-100)进行计算
				 * 		(3)根据公式,要考虑到Java语言和数学计算的差别,
				 * 		   公式中5/9在Java中的值恒等于0,因为5和9都是int类型,计算结果也是int类型,
				 * 		   要想得到5/9的真实近似值,只需要把5改成double类型:5-->5.0
				 * 3.代码编写
				 */
				double F = 37.6;				// 华氏温度
				double C = 5.0 / 9 * (F - 100);	// 摄氏温度
				System.out.println("此时华氏温度为" + F + ",摄氏温度为" + C);
		}
}
/**
 * 看到一个表达式，一定要牢牢记住数学运算和Java中的运算的差别：
 * 1.Java中整数除以整数只能得到整数，会丢失后面的小数点
 * 2.除法运算符要想得到double类型的结果,操作数中一定要一个是(或都是)double类型的值
 */
