public class TernaryOperatorDetail {
		// 三元运算符使用细节
		public static void main(String[] args) {
				// 表达式1和表达式2的返回值要赋给与返回值相同数据类型的变量(或自动转换)
				int a = 3;
				int b = 7;
				int c = a > b ? a : b;
				double d = a > b ? a : b;	// 这样是可以的,因为int类型可以自动转换为double类型
				// int c = a > b ? 1.1 : 2.3;会报类型不兼容错误,因为1.1和2.3是double类型,而c为int类型
				// 可以写成这样：
				// 1. double c = a > b ? 1.1 : 2.3;
				// 2. int c = a > b ? (int)1.1 : (int)2.3;进行强制类型转换
				System.out.println(c);
				System.out.println(d);
		}
}
