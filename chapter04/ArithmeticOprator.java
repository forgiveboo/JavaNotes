public class ArithmeticOprator {
		// 算术运算符的使用
		public static void main(String[] args) {
				// 除号(/)使用
				double d = 10 / 4;
				/*
				 * 这里会输出2.0
				 * 因为10和4是int类型，那么整个表达式的类型是int类型，计算的结果会丢掉小数点
				 * 又因为把表达式的运算结果赋值给了double类型的变量，那么表达式的结果就会自动转换为double类型
				 */
				System.out.println(d);
				double d1 =  10.0 / 4;
				/*
				 * 这里会输出2.5
				 * 因为10.0是double类型，那么整个表达式的数据类型就是double类型，计算的结果就会保留小数点
				 */
				System.out.println(d1);

				// 取余(%)的使用
				/*
				 * 取余(%)的本质：a % b = a - a / b * b
				 */
				System.out.println(10 % 3);		// 输出1
				System.out.println(-10 % 3);	// 输出-1 
				System.out.println(10 % -3);	// 输出1
				System.out.println(-10 % -3);	// 输出i-1

				// 自加(++)的使用
				/*
				 * 自加分为两种：
				 * a++; 先用后加
				 * ++a; 先加后用
				 */
				// 作为独立使用，i++和++i完全等价于i = i + 1;
				int i = 1;
				i++;	// 等价于i = i + 1; 此时i = 1 + 1 = 2
				++i;	// 等价于i = i + 1;	此时i = 2 + 1 = 3
				System.out.println(i);	// 输出3
				/* 
				 * 作为表达式使用时：
				 * i++;是先使用后自加
				 * ++i;是先自加后使用
				 */
				int j = 10;
				int k = j++;			// 等价于int k = j; j = j + 1; 这两条语句(先使用后自加)
				System.out.println("k = " + k + " " + "j = " + j);	// 输出10
				j = 10;					
				int h = ++j;			// 等价于j = j + 1; h = j; 这两条语句(先自加后使用)
				System.out.println("h = " + h +  " " + "j = " + j);;	// 输出11
				/*
				 * 自加和自减的规则完全一样
				 */	
		}
}
