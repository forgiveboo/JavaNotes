import java.util.Scanner;

public class Input {
		// 接收用户输入
		public static void main(String[] agrs) {
				/*
				 * 步骤：
				 * 	(1)导入Scanner类所在的包java.util.*
				 * 	(2)用new关键字创建Scanner对象
				 * 	(3)调用成员方法
				 * Scanner类表示一个简单的文件扫描器
				 */
				Scanner sc = new Scanner(System.in);	// 创建Scanner类的实例
				// 输出提示信息
				System.out.print("请输入名字：");
				// 当程序执行到这里时，会停下来等待用户输入
				String name = sc.next();
				System.out.print("请输入年龄：");
				int age = sc.nextInt();
				System.out.print("请输入薪水：");
				double sal = sc.nextDouble();
				System.out.println("名字：" + name + " 年龄：" + age + " 薪水：" + sal);
				sc.close();
		}
}
