public class TernaryOperatorExercise {
		// 三元运算符练习
		public static void main(String[] args) {
				/*
				 * 1.需求分析：找出三个数中最大的数
				 * 2.思路分析：
				 * 	(1)用三元运算符先找出连个数中最大的数并把结果放入临时变量中
				 * 	(2)再用三云运算符找出临时变量和第三个数中最大的数
				 * 3.写代码
				 */
				int a = 10;
				int b = 8;
				int c = 11;
				int temp = a > b ? a : b;
				int big = temp > c ? temp : c;
				System.out.println(big);
		}
}
