public class ArithmeticOpratorExercise02 {
		// 自加自减练习
		public static void main(String[] args) {
				int i1 = 10;
				int i2 = 20;
				int i = i1++;
				System.out.println("i = " + i);		// 输出10
				System.out.println("i2 = " + i2);	// 输出20
				i = --i2;
				System.out.println("i = " + i);		// 输出19
				System.out.println("i2 = " + i2);	// 输出19
				
		}
}
