public class ArithmeticOpratorExercise01 {
		// 算术运算符练习
		public static void main(String[] args) {
				// 经典面试题1
				int i = 1;
				i = i++;
				/*
				 * i = i++;在计算机中的执行步骤：
				 * 1. 计算机会先定义一个临时变量temp，把i的值赋值给temp，所以此时temp = i = 1(先用)
				 * 2. 再执行i++;此时i = i + 1 = 2(后加)
				 * 3. 最后把temp的值赋值给i，所以此时i = temp = 1
				 * 所以最后变量i的值是1
				 */		
				System.out.println(i);	// 输出1

				// 经典面试题2
				int j = 1;
				j = ++j;
				/*
				 * j = ++j;在计算机中的执行步骤：
				 * 1. 先执行++j;此时j = j + 1 = 2(先加)
				 * 2. 计算机定义一个临时变量temp，把j的值赋值给temp，所以此时temp = j = 2(后用)
				 * 3. 把temp的值赋值给j，此时j = temp = 2
				 * 所以最后变量j的值是2
				 */
				System.out.println(j);	// 输出2
		}
}
/**
 *	这两道题其实考察i++;和++i的用法和区别：
 *	1.在单独使用时i++;和++i;没有任何区别
 *	2.在表达式中使用时一定要记住i++;是先用后加，++i是先加后用
 */
