public class RelationalOperator {
		// 关系运算符的使用
		public static void main(String[] args) {
				// 关系运算符的运算结果只能是boolean类型，要么是true要么是false
				int i = 3;
				int j = 9;
				System.out.println(i == j);	// false
				System.out.println(i != j);	// true
				System.out.println(i > j);	// false
				System.out.println(i >= j);	// false
				System.out.println(i < j);	// true
				System.out.println(i <= j);	// true
				boolean flag = i < j;
				System.out.println(flag);	// true
		}
}
