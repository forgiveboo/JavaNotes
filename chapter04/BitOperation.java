public class BitOperation {
	// 演示位运算
	public static void main(String[] args) {
		int a = 1 >> 2;		// 1向右位移2位
		int b = -1 >> 2;	// -1向右位移2位
		int c = 1 << 2;
		int d = -1 << 2;
		int e = 3 >>> 2;	// 算术右移
		int f = ~2;			// 按位取反
		int g = 2 & 3;		// 2按位与3
		int h = 2 | 3;		// 2按位或3
		int i = -3 ^ 3;		// -3按位异或3
		// 输出a,b,c,d,e,f,g,h,i
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
		System.out.println(d);
		System.out.println(e);
		System.out.println(f);
		System.out.println(g);
		System.out.println(h);
		System.out.println(i);
	}
}
