public class LogicOperator01 {
		// 短路与(&&)和逻辑与(&)的区别
		public static void main(String[] args) {
				/*
				 * 短路与(&&):如果第一个条件为false，那么计算机就不会判断第二个条件，结果直接为false
				 * 	  如果第一个条件为true，计算机才会判断第二个条件是true还是false，如果为true则结果为true，如果为false则结果为fasle
				 * 逻辑与(&):不管第一个条件为false还是true，计算机都会判断第二个条件，再得到结果(运算规则和短路与相同)
				 * &&比&的效率高，所以在开发中基本使用&&
				 */
				// 短路与运算步骤
				int age = 20;
				if (age > 20 && age < 50) {	
						// 判断第一个条件结果为false
						// 第二个条件判断直接跳过
						// 整个关系表达时的结果为false
						// 执行esle语句
						System.out.println(age);
				} else {
						System.out.println("短路与运算步骤");
				}
				// 逻辑与运算步骤
				if (age > 20 & age < 50) {
						// 判断第一个条件结果为false
						// 再判断第二个条件结果为true
						// 整个关系表达时结果为false
						// 执行else语句
						System.out.println(age);
				} else {
						System.out.println("逻辑与运算步骤");
				}
		}
}
