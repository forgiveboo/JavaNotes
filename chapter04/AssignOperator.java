public class AssignOperator {
		// 赋值运算符的使用
		public static void main(String[] agrs) {
				// 基本赋值运算符
				int num = 10;
				num += 10;
				System.out.println(num);
				num /= 4;
				System.out.println(num);

				// 复合赋值运算符
				// 复合赋值运算符会进行强制类型转换
				byte b = 1;
				b += 3;	//等价于-->b = (byte)(b + 2);计算机会把表达式的类型转换为b的数据类型
				b++;	//等价于-->b = (byte)(b + 1);
		}
}
