public class TernaryOperator {
		// 三元运算符
		public static void main(String[] args) {
				/*
				 * 基本语法: 关系表达式?表达式1:表达式2;
				 * 语法规则: 如果关系表达式为true，运算结果为表达式1的执行结果, 反之, 运算结果为表达式2的执行结果
				 */
				int a = 3;
				int b = 5;
				int result = a > b ? a++ : b++;
				System.out.println("result = " + result);	// 这里输出5,因为b++;是先用后加,所以计算机会先把b的值返回b再自加
				System.out.println("a = " + a);	// 这里输出3,因为a > b结果是false,所以a++;不会执行
				System.out.println("b = " + b);	// 这里输出6,因为b++;在b返回自身值后会自加
		}
}
