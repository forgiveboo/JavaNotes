public class InverseOperator {
		// 取反(!)和异或(^)的使用
		public static void main(String[] args) {
				// 取反(!):真变假，假变真
				boolean flag = false;
				if (!flag) {
						System.out.println("真变假，假变真");
				}

				// 异或(^):当a和b不一样时，结果为true，反之则为false
				if (flag ^ !flag) {
						System.out.println("当a和b不一样时，结果为true，反之为false");
				}
		}
}
