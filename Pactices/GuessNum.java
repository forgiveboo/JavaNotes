import java.util.Scanner;

public class GuessNum {
		public static void main(String[] args) {
				// 创建键盘输入对象
				Scanner sc =  new Scanner(System.in);
				System.out.print("请输入1-100的整数：");
				// 产生随机数
				int RandNum = (int)(Math.random() * 100 + 1);
				// 开始猜数字
				while (true) {
						int GuessNum = sc.nextInt();
						if (GuessNum < RandNum) {
								System.out.print("太小了，再猜:");
						} else if (GuessNum > RandNum) {
								System.out.print("小大了，再猜:");
						} else {
								System.out.print("太棒了，猜对了");
								break;
						}
				}
				sc.close();
		}
}
