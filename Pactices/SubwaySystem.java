import java.util.Scanner;

public class SubwaySystem {
		// 模拟地铁系统
		public static void main(String[] args) {
				// 需求：在本文件夹下的需求.png图片中
				/*
				 * 1.需求分析：
				 * 		(1)模拟地铁系统的界面(在图片中)
				 * 		(2)请求用户输入上车车站
				 * 		(3)判断用户输入的车站名称是否正确,如不正确提醒用户重新输入
				 * 		(4)请求用户输入下车车站名称
				 * 		(5)判断用户输入的车站名称是否正确,如不正确提醒用户重新输入
				 * 		(6)计算上车车站到下车车站之间一共有多少站
				 * 		(7)计算一共需要收费多少
				 * 		(8)计算到站需要多长时间
				 * 2.思路分析
				 * 		(1)定义12个String类型变量来保存车站名称
				 * 		(2)输出基本的模拟地铁界面
				 * 		(3)定义2个String类型变量来接收用户输入
				 * 		(4)使用String类中的equals()方法判断用户输入的车站名称是否正确
				 * 		(5)如果用户输入的车站名称有误,提醒用户重新输入
				 * 		(6)假设一站一块钱,走一站需要2分钟，那么定义2个计数器,使用for循环计算收费和用时
				 * 3.上代码
				 */
				// 定义变量存储车站名称
				String s1 = "河南工业大学";		
				String s2 = "郑大科技园";		
				String s3 = "郑州大学";		
				String s4 = "梧桐街";		
				String s5 = "兰寨";		
				String s6 = "铁炉";		
				String s7 = "市民中心";		
				String s8 = "西流湖";		
				String s9 = "西三环";		
				String s10 = "秦岭路";		
				String s11 = "五一公园";		
				String s12 = "碧沙岗";
				// 打印用户界面
				System.out.println("欢迎使用地铁系统！");
				System.out.println("====================");
				System.out.println("第1站：" + s1 + "\t" + "第2站：" + s2 + "\t" + "第3站：" + s3);
				System.out.println("第4站：" + s4 + "\t\t" + "第5站：" + s5 + "\t\t" + "第6站：" + s6);
				System.out.println("第7站：" + s7 + "\t\t" + "第8站：" + s8 + "\t\t" +"第9站：" + s9);
				System.out.println("第10站：" + s10 + "\t\t" + "第11站：" + s11 + "\t" +"第12站：" + s12);
				System.out.println("====================");
				// 接收用户输入的上车车站名称
				System.out.println("请输入上车车站：");
				Scanner sc = new Scanner(System.in);
				while (true) {}
				String BoardingStation = sc.next();
				// 判断用户输入的车站名称是否正确
				if (BoardingStation.equals(s1)) {
						System.out.println("请输入下车车站：");
				} else {
						System.out.println("您输入的车站不存在");
				}
		}
}
