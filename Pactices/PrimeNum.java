import java.util.Scanner;

public class PrimeNum {
		// 判断素数
		public static void main(String[] args) {
				// 创建Scanner对象
				Scanner sc = new Scanner(System.in);
				// 接收用户输入
				System.out.print("请输入一个整数:");
				int num = sc.nextInt();
				sc.close();
				// 计算一共判断了多少次，如果count == 0那么num就是素数
				int count = 0;
				// 判断素数
				for (int i = 2; i < num; i++) {
						if (num % i == 0) {	// 如果num % i ！= 0,那么count++;就不会执行，就意味着i不是num的因子
								count++;	// 如果num % i == 0,那么count就会+1
						} 
				}
				if (count == 0) {	// 意味着num没有除去它本身的因子
						System.out.printf("%d是素数\n", num);
				} else {
						System.out.printf("%d不是素数\n", num);
				}
		}
}
/*
 * 思路在于用一个计数器count来判断i是不是num的因子
 * 如果count == 0,那么就表示i < num 中没有num的因子，那么num就是素数
 * 如果i是num的因子，count就是自加
 */
