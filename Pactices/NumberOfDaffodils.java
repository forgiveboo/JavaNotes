import java.util.Scanner;

public class NumberOfDaffodils {
		// 用户输入一个数，判断一个数是不是水仙花数
		public static void main(String[] args) {
				// 创建Scannner对象
				Scanner sc = new Scanner(System.in);
				// 接收用户输入
				System.out.print("请输入一个三位数：");
				int num = sc.nextInt();
				sc.close();
				// 取出num的个位十位百位
				int gw = num % 10;
				int sw = num / 10 % 10;
				int bw = num / 100;
				if (gw * gw * gw + sw * sw * sw + bw * bw * bw == num) {
						System.out.printf("%d是水仙花数\n", num);
				} else {
						System.out.printf("%d不是水仙花数\n", num);
				}		
		}
}
